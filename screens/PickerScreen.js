import React, { Component } from 'react';
import { View, Text, Picker, StyleSheet } from 'react-native'

export default class PickerScreen extends Component{
   state = {user: ''}
   updateUser = (user) => {
      this.setState({ user: user })
   }
   render() {
      return (
         <View>
            <Picker selectedValue = {this.state.user} onValueChange = {this.updateUser}>
               <Picker.Item label = "Veejay" value = "Veejay" />
               <Picker.Item label = "Burlaza" value = "Burlaza" />
               <Picker.Item label = "Manito" value = "Manito" />
            </Picker>
            <Text style = {styles.text}>{this.state.user}</Text>
         </View>
      )
   }
}
const styles = StyleSheet.create({
   text: {
      fontSize: 30,
      alignSelf: 'center',
      color: 'red'
   }
})