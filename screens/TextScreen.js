import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';

export default class TextScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      titleText: "Akong kaagi",
      bodyText: 'Dili jud lalim mahimog cute bai, unsaon man ni?'
    };
  }

  render() {
    return (
      <Text style={styles.baseText}>
        <Text style={styles.titleText} onPress={this.onPressTitle}>
          {this.state.titleText}{'\n'}{'\n'}
        </Text>
        <Text numberOfLines={5}>
          {this.state.bodyText}
        </Text>
      </Text>
    );
  }
}

const styles = StyleSheet.create({
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});
