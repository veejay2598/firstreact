import { Button, View, StyleSheet, Alert} from 'react-native'
import React, { Component } from 'react'

export default class ButtonScreen extends Component {
    render() {
      return (
        <View style={[styles.container]}>
            <Button
                onPress={() => Alert.alert('Say', 'Hello :D')}
                title="Click Me"
                color="#841584"
                accessibilityLabel="Learn more about this purple button"
            />
        </View>
      )
    }
  }

  const styles = StyleSheet.create({
    container: {        
      flex: 0,
      justifyContent: 'center'
    }
  })