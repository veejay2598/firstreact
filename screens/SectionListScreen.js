import React, {Component} from 'react'
import {
View,
SectionList,
Text
}from 'react-native'

export default class SectionListScreen extends Component {
    render(){
        return(
           <View>
                <SectionList
                renderItem={({item, index}) => <Text key={index}>{item}</Text>}
                renderSectionHeader={({section: {title}}) => (
                    <Text style={{fontWeight: 'bold'}}>{title}</Text>
                )}
                sections={[
                    {title: 'Isda', data: ['Tilapia', 'Matambaka']},
                    {title: 'Iro', data: ['Askal', 'Aspin']},
                    {title: 'Iring', data: ['Foreigner', 'Local']},
                ]}
                keyExtractor={(item, index) => item + index}
                /> 
           </View>
        );
    }
}