import React, { Component } from 'react';
import { StyleSheet, View, Button, ScrollView } from 'react-native';

export default class HomeScreen extends Component {
    render() {
        return (
          <ScrollView>
          <View style={styles.container}>
            <Button
              title="Go to Details"
              onPress={() => this.props.navigation.navigate('Details')}
            />
            <Button
              title="Go to Activity Indicator"
              onPress={() => this.props.navigation.navigate('Activity')}
            />
             <Button
              title="Go to Button Screen "
              onPress={() => this.props.navigation.navigate('Button')}
            />
            <Button
              title="Go to Image Screen "
              onPress={() => this.props.navigation.navigate('Image')}
            />
            <Button
              title="Go to List View "
              onPress={() => this.props.navigation.navigate('ListView')}
            />
            <Button
              title="Go to Modal Screen "
              onPress={() => this.props.navigation.navigate('ModalView')}
            />
            <Button
              title="Go to Picker Screen "
              onPress={() => this.props.navigation.navigate('Picker')}
            />
            <Button
              title="Go to Progress Bar Screen "
              onPress={() => this.props.navigation.navigate('ProgressBar')}
            />
            <Button
              title="Go to Scroll View Screen "
              onPress={() => this.props.navigation.navigate('ScrollView')}
            />
            <Button
              title="Go to SectionList Screen "
              onPress={() => this.props.navigation.navigate('SectionList')}
            />
            <Button
              title="Go to Slider Screen "
              onPress={() => this.props.navigation.navigate('Slider')}
            />
            <Button
              title="Go to Status Bar Screen "
              onPress={() => this.props.navigation.navigate('StatusBar')}
            />
            <Button
              title="Go to Switch Screen "
              onPress={() => this.props.navigation.navigate('Switch')}
            />
            <Button
              title="Go to Text Input Screen "
              onPress={() => this.props.navigation.navigate('TextInput')}
            />
            <Button
              title="Go to Text Screen "
              onPress={() => this.props.navigation.navigate('Text')}
            />
            <Button
              title="Go to Touchable Highlight Screen "
              onPress={() => this.props.navigation.navigate('TouchableHighlight')}
            />
            <Button
              title="Go to Touchable Native Feedback Screen "
              onPress={() => this.props.navigation.navigate('TouchFeedback')}
            />
            <Button
              title="Go to Touchable Opacity Screen "
              onPress={() => this.props.navigation.navigate('TouchableOpacity')}
            />
            <Button
              title="Go to Touchable without Feedback Screen "
              onPress={() => this.props.navigation.navigate('TouchableWithoutFeedback')}
            />
            <Button
              title="Go to View pager Android Screen "
              onPress={() => this.props.navigation.navigate('ViewPagerAndroid')}
            />
            <Button
              title="Go to View Screen "
              onPress={() => this.props.navigation.navigate('ViewScreen')}
            />


          </View>
          </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });