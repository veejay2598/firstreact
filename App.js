 
import React from 'react';
import { createStackNavigator } from 'react-navigation';
import HomeScreen from './screens/HomeScreen'
import DetailsScreen from './screens/DetailsScreen'
import ActivityIndicator from './screens/ActivityIndicatorScreen'
import ButtonScreen from './screens/ButtonScreen'
import ImageScreen from './screens/ImageScreen'
import ListViewScreen from './screens/ListViewScreen'
import ModalViewScreen from './screens/ModalViewScreen'
import PickerScreen from './screens/PickerScreen'
import ProgressBarScreen from './screens/ProgressBarScreen'
import RefreshControlScreen from './screens/RefreshControlScreen'
import ScrollViewScreen from './screens/ScrollViewScreen'
import SectionListScreen from './screens/SectionListScreen'
import SliderScreen from './screens/SliderScreen'
import StatusBarScreen from './screens/StatusBarScreen'
import SwitchScreen from './screens/SwitchScreen'
import TextInputScreen from './screens/TextInputScreen'
import TextScreen from './screens/TextScreen'
import TouchableHighlightScreen from './screens/TouchableHighlightScreen'
import TouchableNativeFeedbackScreen from './screens/TouchableNativeFeedbackScreen'
import TouchableOpacityScreen from './screens/TouchableOpacityScreen'
import TouchableWithoutFeedbackScreen from './screens/TouchableWithoutFeedbackScreen'
import ViewPagerAndroidScreen from './screens/ViewPagerAndroidScreen'
import ViewScreen from './screens/ViewScreen'

const RootStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    Details: {
      screen: DetailsScreen,
    },
    Activity: {
      screen: ActivityIndicator,
    },
    Button: {
      screen:  ButtonScreen,
    },
    Image: {
      screen: ImageScreen,
    },
    ListView: {
      screen: ListViewScreen,
    },
    ModalView: {
      screen: ModalViewScreen,
    },
    Picker: {
      screen: PickerScreen,
    },
    ProgressBar: {
      screen: ProgressBarScreen,
    },
    RefreshControl: {
      screen: RefreshControlScreen,
    },
    ScrollView: {
      screen: ScrollViewScreen,
    },
    SectionList: {
      screen: SectionListScreen,
    },
    Slider: {
      screen: SliderScreen,
    },
    StatusBar: {
      screen: StatusBarScreen,
    },
    Switch: {
      screen: SwitchScreen,
    },
    TextInput: {
      screen: TextInputScreen,
    },
    Text: {
      screen: TextScreen,
    },
    TouchableHighlight: {
      screen: TouchableHighlightScreen,
    },
    TouchFeedback: {
      screen: TouchableNativeFeedbackScreen,
    },
    TouchableOpacity: {
      screen: TouchableOpacityScreen,
    },
    TouchableWithoutFeedback: {
      screen: TouchableWithoutFeedbackScreen,
    },
    ViewPagerAndroid: {
      screen: ViewPagerAndroidScreen,
    },
    ViewScreen: {
      screen: ViewScreen,
    },
  },

  {
    initialRouteName: 'Home',
  }
);

export default class App extends React.Component {
  render(){
    return <RootStack />;
  }
}